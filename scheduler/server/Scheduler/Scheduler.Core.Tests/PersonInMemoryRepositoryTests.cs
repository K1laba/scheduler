using System;
using System.Linq;
using Xunit;

namespace Scheduler.Core.Tests
{
    public class PersonInMemoryRepositoryTests
    {
        private readonly PersonInMemoryRepository _repo;

        public PersonInMemoryRepositoryTests()
        {
            _repo = new PersonInMemoryRepository();
        }
        [Theory]
        [InlineData(10, 10)]
        [InlineData(100, 100)]
        public void Get_WhenCalls_ShouldReturnCorrectResults(int requested, int expected)
        {
            //act
            var actual = _repo.Get(requested).Count();
            //assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(10, "Person 01")]
        [InlineData(100, "Person 001")]
        [InlineData(1000, "Person 0001")]
        public void Get_WhenCalls_ShouldReturnPersonNamesInCorrectFormat(int totalPersons, string expectedNameFormat)
        {
            //act
            var result = _repo.Get(totalPersons);
            //assert
            Assert.Equal(expectedNameFormat, result.First().Name);
        }
    }
}
