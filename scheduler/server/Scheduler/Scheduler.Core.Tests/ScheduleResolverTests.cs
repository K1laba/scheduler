﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Scheduler.Core.Tests
{
    public class ScheduleResolverTests
    {
        private PersonInMemoryRepository _repo;
        private SchedulerResolver _resolver;

        public ScheduleResolverTests()
        {
            _repo = new PersonInMemoryRepository();
            _resolver = new SchedulerResolver(_repo);
        }
        [Fact]
        public void GetSchedule_WhenResolvesSchedule_ShouldNoneOfThePersonsWorkFullDay()
        {
            //arrange
            int expected = 1;
            //act
            int actual = _resolver.GetSchedule().GroupBy(s => new
            {
                s.PersonId,
                s.StartDate.Date
            }).Select(i => i.Count()).Max();
            //assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void GetSchedule_WhenResolvesSchedule_ShouldAllPersonsHaveWorkedWholeDayInTwoWeeks()
        {
            //arrange
            int expected = 2;
            //act
            var result = _resolver.GetSchedule();
            //assert
            Assert.True(result.GroupBy(s => s.PersonId).All(c => c.Count() == expected));
        }
        [Fact]
        public void GetSchedule_WhenResolvesSchedule_ShouldNoneOfPersonWorkConsecutiveDays()
        {
            //arrange
            int daysBetweenShifts = 7;
            //act
            var result = _resolver.GetSchedule();
            //assert
            Assert.True(result.GroupBy(s => s.PersonId)
                              .All(c => Math.Round(c.Max(i => i.StartDate.Date).Subtract
                                       (c.Min(i => i.StartDate.Date)).TotalDays) == daysBetweenShifts));
        }
        [Fact]
        public void GetSchedule_WhenResolvesSchedule_ShouldExcludeWeekends()
        {
            //arrange
            List<int> expectedWeekDays = new List<int>() { 1, 2, 3, 4, 5 };
            //act
            var result = _resolver.GetSchedule();
            //assert
            Assert.True(result.All(r => expectedWeekDays.Any(e => e == (int)r.StartDate.DayOfWeek)));
        }
        [Fact]
        public void GetSchedule_WhenResolves_ShouldReturnTwoPersonsForEachDay()
        {
            //arrange
            int numberOfPersons = 10;
            int expected = 2;
            //act
            var result = _resolver.GetSchedule(numberOfPersons);
            //assert
            Assert.True(result.GroupBy(i => i.StartDate.Date).All(e => e.Count() == expected));
        }
    }
}
