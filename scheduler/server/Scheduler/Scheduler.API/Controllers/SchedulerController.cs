﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Scheduler.Core;

namespace Scheduler.API.Controllers
{
    [Route("api/[controller]")]
    public class ScheduleController : Controller
    {
        private SchedulerResolver _scheduler;

        public ScheduleController(SchedulerResolver scheduler)
        {
            _scheduler = scheduler ?? throw new ArgumentNullException(nameof(scheduler));
        }
        // GET api/schedule
        [HttpGet]
        public IEnumerable<ScheduleItem> Get()
        {
            return _scheduler.GetSchedule();
        }
    }
}
