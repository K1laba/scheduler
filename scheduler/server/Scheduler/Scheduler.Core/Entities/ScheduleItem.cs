﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Core
{
    public class ScheduleItem
    {
        public ScheduleItem() { }
        public ScheduleItem(ScheduleItem scheduleItem)
        {
            this.PersonId = scheduleItem.PersonId;
            this.PersonName = scheduleItem.PersonName;
            this.Color = scheduleItem.Color;
        }

        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Color Color { get; set; }
    }
}
