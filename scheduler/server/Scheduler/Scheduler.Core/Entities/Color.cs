﻿namespace Scheduler.Core
{
    public class Color
    {
        public string Primary { get; set; }
        public string Secondary { get; set; }

        public static Color Red()
        {
            return new Color() { Primary = "#ad2121", Secondary = "#FAE3E3" };
        }
        public static Color Blue()
        {
            return new Color() { Primary = "#1e90ff", Secondary = "#D1E8FF" };
        }
    }
}
