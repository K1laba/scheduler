﻿using System.Collections.Generic;

namespace Scheduler.Core
{
    public class PersonInMemoryRepository : IPersonRepository
    {
        public IEnumerable<Person> Get(int numberOfPersons)
        {
            var result = new List<Person>();
            int digitsLength = numberOfPersons.ToString().Length;
            for (int i = 1; i <= numberOfPersons; i++)
            {
                string format = $"d{digitsLength}";
                result.Add(new Person { Id = i, Name = $"Person {i.ToString(format)}" });
            }
            return result;
        }
    }
}
