﻿using System;
using System.Collections.Generic;

namespace Scheduler.Core
{
    public interface IPersonRepository
    {
        IEnumerable<Person> Get(int numberOfPersons);
    }
}
