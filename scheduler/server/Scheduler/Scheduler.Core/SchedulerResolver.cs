﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Scheduler.Core
{
    public class SchedulerResolver
    {
        private const int NUMBER_OF_PERSONS_PER_DAY = 2;
        private const int SHIFT_DURATION_IN_HOURS = 4;
        private const int WORKING_HOURS_STARTS_FROM = 9;
        private readonly IPersonRepository _repo;

        public SchedulerResolver(IPersonRepository repo)
        {
            _repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }
        public IEnumerable<ScheduleItem> GetSchedule(int numberOfPersons = 10)
        {
            var persons = _repo.Get(numberOfPersons);
            persons = ShufflePersons(persons);
            DateTime closestStartTime = GetClosestStartTime(DateTime.Now);

            var schedule1 = CreateSchedule(persons, closestStartTime);
            var schedule2 = CreateSchedule(persons, schedule1.Last().StartDate.AddDays(1));
            return schedule1.Concat(schedule2);
        }

        private List<ScheduleItem> CreateSchedule(IEnumerable<Person> persons, DateTime closestStartTime)
        {
            return persons.Aggregate(new List<ScheduleItem>(), (list, current) =>
            {
                int personsSoFarPerDay = list.Count % NUMBER_OF_PERSONS_PER_DAY;
                int daysToAdd = list.Count / NUMBER_OF_PERSONS_PER_DAY;
                var startTime = closestStartTime.AddHours(personsSoFarPerDay * SHIFT_DURATION_IN_HOURS);

                startTime = GetWeekDay(startTime, daysToAdd);
                list.Add(new ScheduleItem
                {
                    PersonId = current.Id,
                    PersonName = current.Name,
                    Color = list.Count % NUMBER_OF_PERSONS_PER_DAY == 0 ? Color.Red() : Color.Blue(),
                    StartDate = startTime,
                    EndDate = startTime.AddHours(SHIFT_DURATION_IN_HOURS)
                });
                return list;
            }, result => result);
        }

        private DateTime GetWeekDay(DateTime startTime, int daysToAdd)
        {
            for (int i = 0; i < daysToAdd; i++)
            {
                startTime = GetNearestWorkingDay(startTime.AddDays(1));
            }

            return startTime;
        }

        private IEnumerable<Person> ShufflePersons(IEnumerable<Person> persons)
        {
            Random rand = new Random();
            return persons.OrderBy(c => rand.Next()).ToList();
        }

        private DateTime GetClosestStartTime(DateTime fromDate)
        {
            DateTime closestStartTime = GetNearestWorkingDay(fromDate.Date);
            closestStartTime = closestStartTime.Add(new TimeSpan(WORKING_HOURS_STARTS_FROM, 0, 0));
            return closestStartTime;
        }

        private DateTime GetNearestWorkingDay(DateTime date)
        {
            var startDate = new DateTime(date.Ticks);
            while (IsWeekend(startDate)) { startDate = startDate.AddDays(1); }
            return startDate;
        }

        private bool IsWeekend(DateTime date)
        {
            var weekends = new List<int> { 0, 6 };
            return weekends.Any(d => d == (int)date.DayOfWeek);
        }
    }
}