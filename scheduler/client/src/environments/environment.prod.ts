var serverBaseAddress = "http://localhost:3000";
var apiBaseAddress = serverBaseAddress + "/api";

export const environment = {
  production: false,
  apiBaseAddress: apiBaseAddress
};