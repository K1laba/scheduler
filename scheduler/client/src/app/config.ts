import { environment } from '../environments/environment';
export const CONFIG = {
    getApiBaseUrl(): string {
        return environment.apiBaseAddress;
    }
}