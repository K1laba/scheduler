import {
  Component,
  ChangeDetectionStrategy,
  ViewEncapsulation, OnInit
} from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { subDays, addDays, addHours } from 'date-fns';
import { colors } from '../colors';
import { ScheduleService } from '../services/schedule.service';

@Component({
  selector: 'schedule-component',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'schedule.component.html'
})
export class ScheduleComponent implements OnInit {
  public view: string = 'month';
  public viewDate: Date = new Date();
  public excludeDays: number[] = [0, 6];
  public events: CalendarEvent[] = [];
  constructor(private service: ScheduleService) {

  }
  ngOnInit() {
    this.service.get().subscribe(res => this.events = res);
  }
  public handleChange(direction: 'back' | 'forward'): void {
    if (this.view === 'day') {
      if (direction === 'back') {
        while (this.excludeDays.indexOf(this.viewDate.getDay()) > -1) {
          this.viewDate = subDays(this.viewDate, 1);
        }
      } else if (direction === 'forward') {
        while (this.excludeDays.indexOf(this.viewDate.getDay()) > -1) {
          this.viewDate = addDays(this.viewDate, 1);
        }
      }
    }
  }
}