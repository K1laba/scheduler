import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { CalendarEvent } from 'angular-calendar';
import { CONFIG } from '../config';
import 'rxjs';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ScheduleService {

  private baseUrl: string;
  private requestOptions: RequestOptions;
  private headers: Headers;
  constructor(private http: Http) {
    this.headers = new Headers({
      "Accept": "application/json",
      "Content-Type": "application/json"
    });
    this.requestOptions = new RequestOptions({ headers: this.headers });
    this.baseUrl = CONFIG.getApiBaseUrl();
  }
  public get(): Observable<CalendarEvent[]> {
    return this.http.get(`${this.baseUrl}/schedule`, this.requestOptions)
      .map(res => res.json().map(item => {
        return {
          start: new Date(item.startDate),
          end: new Date(item.endDate),
          title: item.personName,
          color: item.color
        };
      }));
  }

}
