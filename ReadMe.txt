There are two main projects

server - written in asp.net webapi core
in order to start this project just click on F5 in visual studio. That's it

client - written in angular 
in order to start this project firstly you need to run: "npm install" command to restore packages.
after that you can run "ng serve -o" command, which will start application and gets data from server.
That's it.